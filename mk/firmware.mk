# SPDX-License-Identifier: ISC

EFI?=		2023.05

.PHONY: firmware

firmware: firmware/OVMF_CODE.fd firmware/OVMF_VARS.fd
firmware: firmware/QEMU_EFI.fd firmware/QEMU_VARS.fd

firmware/OVMF_CODE.fd firmware/OVMF_VARS.fd:
	curl -sL ${_EFI_AMD64} > ${_TMP}/amd64.deb
	cd ${_TMP}; ar x amd64.deb
	cd ${_TMP}; tar xzf data.tar.xz
	mkdir -p ${@D}
	cp -f ${_TMP}/usr/share/OVMF/OVMF_{CODE,VARS}.fd ${@D}/

firmware/QEMU_EFI.fd:
	curl -sL ${_EFI_ARM64} > ${_TMP}/packer-arm64.deb
	cd ${_TMP}; ar x packer-arm64.deb
	cd ${_TMP}; tar xzf data.tar.xz
	mkdir -p ${@D}
	cp ${_TMP}/usr/share/qemu-efi-aarch64/QEMU_EFI.fd ${@}
	truncate -s 64M ${@}

firmware/QEMU_VARS.fd:
	mkdir -p ${@D}
	dd if=/dev/zero of=${@} bs=1m count=64
