#!/bin/sh
# SPDX-License-Identifier: ISC

while true; do
	pgrep -qxf '/bin/ksh .*reorder_kernel' || break
	sleep 1
done

syspatch
