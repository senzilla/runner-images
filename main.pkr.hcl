# SPDX-License-Identifier: ISC

packer {
	required_plugins {
		qemu = {
			source = "github.com/hashicorp/qemu"
			version = "~> 1"
		}
	}
}

variable "host" {
	type = string
	default = "Darwin.arm64"
}

variable "arch" {
	type = object({
		id = string
		qemu = string
		efi_code = string
		efi_vars = string
		steps = list(list(string))
	})
}

variable "guest" {
	type = object({
		id = string
		iso = map(object({url = string, checksum = string, image = bool}))
		steps = list(list(string))
	})
}

variable "machine" {
	type = map(map(object({arg = string, cpu = string})))
}

variable "build" {
	type = object({size = string, authorized_keys = string})
	default = {
		size = "32G"
		authorized_keys = "provision/authorized_keys"
	}
}

source "qemu" "image" {
	qemu_binary		= var.arch.qemu
	efi_firmware_code	= var.arch.efi_code
	efi_firmware_vars	= var.arch.efi_vars

	qemuargs		= [["-machine", var.machine[var.arch.id][var.host].arg]]
	cpu_model		= var.machine[var.arch.id][var.host].cpu

	iso_url			= var.guest.iso[var.arch.id].url
	iso_checksum		= var.guest.iso[var.arch.id].checksum
	disk_image		= var.guest.iso[var.arch.id].image
	boot_steps		= concat(var.arch.steps, var.guest.steps)

	disk_size		= var.build.size

	output_directory	= "output/${var.guest.id}.${var.arch.id}"
	http_directory		= "htdocs"
	http_port_min		= 80
	http_port_max		= 80
	use_default_display	= true
	ssh_username		= "root"
	ssh_private_key_file	= "provision/temporary"
	boot_wait		= "-1s"
	shutdown_command	= file("provision/shutdown.sh")
}

build {
	sources = ["qemu.image"]

	provisioner "file" {
		source = var.build.authorized_keys
		destination = "/tmp/authorized_keys"
	}

	provisioner "shell" {
		scripts = ["provision/${var.guest.id}.sh"]
	}
}
