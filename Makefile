# SPDX-License-Identifier: ISC

O=		output

ARCHS?=		amd64 arm64
ARCH?=		amd64

SYSTEMS?=	OpenBSD NetBSD
SYSTEM?=	OpenBSD

_TMP!=		mktemp -d /tmp/sz.XXXX

.MAIN: ${SYSTEM}.${ARCH}

.include "mk/firmware.mk"

.for _A in ${ARCHS}

.for _S in ${SYSTEMS}

_OUT=	${O}/${_S}.${_A}
_KEY=	${_S}.${_A}

.PHONY: ${_KEY}
${_KEY}: ${_OUT}/packer-image

${_OUT}/packer-image:
	PACKER_LOG=1 packer build \
		   -var-file=vars/${@D:S/${O}\///:R}.json \
		   -var-file=vars/${@D:S/${O}\///:E}.json \
		   -var-file=vars/machine.json \
		   main.pkr.hcl

.endfor
.endfor
